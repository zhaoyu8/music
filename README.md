# 仿网易云音乐 小程序暗黑模式

## 基于 taro 编写

1. 目前支持微信小程序和 h5

   - h5 的体验没有微信小程序好，因为 taro 对微信小程序支持的最好

2. api 接口来自[网易云音乐 API](https://binaryify.github.io/NeteaseCloudMusicApi/#/?id=%e5%88%9d%e5%a7%8b%e5%8c%96%e6%98%b5%e7%a7%b0)

## 图片展示

<img src="https://www.jianguoyun.com/c/tblv2/DU30qqzNNGSvY42zmNDg6SOsJ98IqnPHsZOFDgdJfPrYF_lEAFxRD_9OIuuPFVinGxwp71sg/EkH9HfmvbuJf9NohuzPJVQ/l" width="300"  height="500" />
<br>
<br>
<img src="https://www.jianguoyun.com/c/tblv2/uRsMHl6AtyUY02tFmCN3V44gtmnTNdYWQvEAsjUT055Tkdva39OusWtfEcRqVRnDUfu9AUQO/zRNN4JAO8pBW8wTGWbshWg/l" width="300"  height="500" />
<br>
<br>

<img src="https://www.jianguoyun.com/c/tblv2/SfJxoLew7q4Eeyfta0LCh5pHboVKa3UEGnJ2igAKWy3sGOPf8u7xaQMWAdd2TESqT-9lqwZ1/0PUi5ejJKzPBExMpIrHVpw/l" width="300"  height="500"/>


# 更新日志
### 1.1.0 | 2020.07.01
- 正式增加记录。工作计划--- 首页增加新歌，新碟。
- 解决歌单详情 歌曲名称超长变形问题